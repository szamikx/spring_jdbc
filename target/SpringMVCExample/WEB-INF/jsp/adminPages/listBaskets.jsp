<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 16.07.2018
  Time: 16:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Baskets list</title>
</head>
<body>
    <div>
        <h2>Categories</h2>
        <a href="<c:url value='/admin/listUsers' />" >Users</a><br>
        <a href="<c:url value='/admin/listBaskets' />" >Baskets</a><br>
        <a href="<c:url value='/admin/listProducts' />" >Products</a><br>
        <a href="<c:url value='/admin/listPictures/' />" >Pictures</a><br>
        <a href="<c:url value='/admin/listActions/' />" >Actions</a><br>
        <a href="<c:url value='/admin/order/listOrders/' />" >List Orders</a><br>
    </div>

    <div>
        <table border="1">
            <tr>
                <th width="80">Basket ID</th>
                <th>Basket's user name</th>
                <th>Basket's user id</th>
                <th>Items</th>
                <%--<th>Order status</th>--%>
                <th>Actions with basket</th>
                <c:if test="${listBaskets.size() > 0}">
            </tr>
            <c:forEach items="${listBaskets}" var="basket">
                <tr>
                    <td>${basket.id}</td>
                    <td>${basket.user.name}</td>
                    <td>${basket.user.id}</td>
                    <td>
                        <c:forEach items="${basket.lineItems}" var="item">
                            Id:${item.id}&nbsp;&nbsp;
                            ${item.product.nameOfProduct}&nbsp;&nbsp;
                            Qty:${item.quantity}&nbsp;

                            <%--Changing quantity for this particular item--%>
                            <form:form method="POST" action="changeQuantity/${basket.id}/${item.product.nameOfProduct}">
                                <input type="text" name="quantity" id="quantity"/>
                                <input type="submit" value="Update"/>&nbsp;
                                <a href="<c:url value='removeItemFromBasket/${item.id}' />" >Remove</a>
                            </form:form>


                            <br>
                        </c:forEach>
                        <br><br>
                        <div id="Adding products to basket">
                            Add Item to basket:
                            <form:form method="POST" action="addItemToBasket/${basket.id}">
                                <select name="product">
                                    <option value="${selected}" selected>${selected}</option>
                                    <c:forEach items="${products}" var="product">
                                        <c:if test="${product != selected}">
                                            <option value="${product.nameOfProduct}">${product.nameOfProduct}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>

                                <input type="submit" value="Select">
                            </form:form>
                        </div>
                    </td>
                    <%--<td>${basket.orderStatus}--%>
                        <%--Change order status to:--%>
                        <%--<form:form method="POST" action="changeOrderStatus/${basket.id}">--%>
                            <%--<select name="status">--%>
                                <%--<option value="${selected}" selected>${selected}</option>--%>
                                <%--<c:forEach items="${statuses}" var="status">--%>
                                    <%--<c:if test="${status != selected}">--%>
                                        <%--<option value="${status}">${status}</option>--%>
                                    <%--</c:if>--%>
                                <%--</c:forEach>--%>
                            <%--</select>--%>

                            <%--<input type="submit" value="Select">--%>
                        <%--</form:form>--%>
                    <%--</td>--%>
                    <td>
                        <a href="<c:url value='removeBasket/${basket.id}' />" >Remove basket</a>
                        <br>
                        <a href="<c:url value='order/makeOrderFromBasket/${basket.id}' />" >Make order</a>
                    </td>
                </tr>
            </c:forEach>
            </c:if>
            <c:if test="${listBaskets.size()==0}">
                There are no baskets
            </c:if>
        </table>
    </div>


</body>
</html>
