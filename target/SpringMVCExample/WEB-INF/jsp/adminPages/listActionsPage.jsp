<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 17.07.2018
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of Actions</title>
</head>
<body>
    <div>
        <h2>Categories</h2>
        <a href="<c:url value='/admin/listUsers' />" >Users</a><br>
        <a href="<c:url value='/admin/listBaskets' />" >Baskets</a><br>
        <a href="<c:url value='/admin/listProducts' />" >Products</a><br>
        <a href="<c:url value='/admin/listPictures/' />" >Pictures</a><br>
        <a href="<c:url value='/admin/listActions/listActionsPage.jsp' />" >Actions</a><br>
        <a href="<c:url value='/admin/order/listOrders/' />" >List Orders</a><br>
    </div>

    <div>
        Actions
        <table border="1">
            <tr>
                <th width="80">Action ID</th>
                <th>Action name</th>
                <th>Action description</th>
                <th>Action discount percent</th>
                <th>Action quantity</th>
                <th>Products in action</th>
                <c:if test="${listActions.size() > 0}">
            </tr>
            <c:forEach items="${listActions}" var="action">
                <tr>
                    <td>${action.id}</td>
                    <td>${action.name}</td>
                    <td>${action.description}</td>
                    <td>${action.discountPercent}</td>
                    <td>${action.quantity}</td>
                    <td>
                        <c:forEach items="${action.products}" var="item">
                            ${item.nameOfProduct}
                            &nbsp;
                            <a href="<c:url value='/admin/removeProductFromAction/${action.id}/${item.id}' />" >
                                Remove product
                            </a>
                            <br>
                        </c:forEach>


                        <div id="Adding products to action">
                            Add product to action:
                            <form:form method="POST" action="/admin/addProductToAction/${action.id}" >
                            <select name="product">
                                    <option value="${selected}" selected>${selected}</option>
                                    <c:forEach items="${products}" var="product">
                                        <c:if test="${product != selected}">
                                            <option value="${product.nameOfProduct}">${product.nameOfProduct}</option>
                                        </c:if>

                                    </c:forEach>
                                </select>

                                <input type="submit" value="Select">
                            </form:form>
                        </div>

                    </td>
                    <td>
                        <a href="<c:url value='/admin/removeAction/${action.id}' />" >Remove action</a>
                        <br>
                        <a href="<c:url value='/admin/editAction/${action.id}' />" >Edit action</a>
                    </td>
                </tr>
            </c:forEach>
            </c:if>
            <c:if test="${listActions.size()==0}">
                There are no actions
            </c:if>
        </table>

<br><br>
    </div>
    <div id="addAction">
        <a href="<c:url value='/admin/addAction/' />" >Add action</a>
            <form:form method="POST" modelAttribute="action1" action="/admin/addAction/">
                <table border="1">
                    <tr>
                        <td><label for="name">Name: </label> </td>
                        <td><form:input path="name" id="name"/></td>
                    </tr>

                    <tr>
                        <td><label for="description">Description of Action: </label> </td>
                        <td><form:input path="description" id="description"/></td>
                    </tr>

                    <tr>
                        <td><label for="discountPercent">discountPercent: </label> </td>
                        <td><form:input path="discountPercent" id="discountPercent"/></td>
                    </tr>

                    <tr>
                        <td><label for="quantity">Quantity: </label> </td>
                        <td><form:input path="quantity" id="quantity"/></td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Add Action"/>
                        </td>
                    </tr>
                </table>


            </form:form>

    </div>

</body>
</html>
