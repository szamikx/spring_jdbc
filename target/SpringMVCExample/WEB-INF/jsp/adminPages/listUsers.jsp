<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 16.07.2018
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of pictures</title>
</head>
<body>
<div>
    <h2>Categories</h2>
    <a href="<c:url value='/admin/listUsers' />" >Users</a><br>
    <a href="<c:url value='/admin/listBaskets' />" >Baskets</a><br>
    <a href="<c:url value='/admin/listProducts' />" >Products</a><br>
    <a href="<c:url value='/admin/listPictures/' />" >Pictures</a><br>
    <a href="<c:url value='/admin/listActions/' />" >Actions</a><br>
    <a href="<c:url value='/admin/order/listOrders/' />" >List Orders</a><br>
</div>

<div>
    <table border="1">
        <tr>
            <th width="80">User ID</th>
            <th>User name</th>
            <th>User e-mail</th>
            <%--<th>User basket Id</th>--%>
            <th>User role</th>
            <th>User status</th>
            <th>Actions with user</th>
            <c:if test="${listUsers.size() > 0}">
        </tr>
        <c:forEach items="${listUsers}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.email}</td>
                <%--<td>${user.basket.id}</td>--%>
                <td>${user.role}</td>
                <td>${user.status}</td>
                <td><a href="<c:url value='removeUser/${user.id}' />" >Delete user</a>
                    <c:if test="${user.status=='Active'}">
                        <br> <a href="<c:url value='deactivateUser/${user.id}' />" >Deactivate user</a>
                    </c:if>
                    <c:if test="${user.status=='NonActive'}">
                        <br> <a href="<c:url value='activateUser/${user.id}' />" >Activate user</a>
                    </c:if>
                    <%--<br> <a href="<c:url value='deactivateUser/${user.id}' />" >Deactivate user</a>--%>
                    <%--<br> <a href="<c:url value='changeRoleUserAdmin/${user.id}' />" >Change role</a>--%>

                    <form:form method="POST" action="changeRole/${user.id}">
                        <select name="roles">
                            <option value="Admin">Admin</option>
                            <option value="Manager">Manager</option>
                            <option value="User">User</option>
                            <option value="Guest">Guest</option>
                        </select>
                        <br><br>
                        <input type="submit" value="Change Role">
                    </form:form>








                </td>
            </tr>
        </c:forEach>
        </c:if>
        <c:if test="${listUsers.size()==0}">
            There are no users
        </c:if>
    </table>
    <div><a href="<c:url value='addUser' />" >Add user</a></div>
</div>
</body>
</html>
