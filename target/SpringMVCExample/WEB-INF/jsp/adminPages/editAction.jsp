<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 03.08.2018
  Time: 13:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit action</title>
</head>
<body>
<div id="addAction">

    <form:form method="POST" modelAttribute="action" action="/admin/editAction/">
        <table border="1">
            <tr>
                <td><label for="name">Name: </label> </td>
                <td><form:input path="name" id="name"/></td>
            </tr>

            <tr>
                <td><label for="description">Description of Action: </label> </td>
                <td><form:input path="description" id="description"/></td>
            </tr>

            <tr>
                <td><label for="discountPercent">discountPercent: </label> </td>
                <td><form:input path="discountPercent" id="discountPercent"/></td>
            </tr>

            <tr>
                <td><label for="quantity">Quantity: </label> </td>
                <td><form:input path="quantity" id="quantity"/></td>
            </tr>

            <tr>
                <td colspan="3">
                    <input type="submit" value="Edit Action"/>
                </td>
            </tr>
        </table>
    </form:form>

</div>

</body>
</html>
