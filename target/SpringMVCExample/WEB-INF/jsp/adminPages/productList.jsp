<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 16.07.2018
  Time: 14:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product list</title>
</head>
<body>
    <div>
        <h2>Categories</h2>
        <a href="<c:url value='/admin/listUsers' />" >Users</a><br>
        <a href="<c:url value='/admin/listBaskets' />" >Baskets</a><br>
        <a href="<c:url value='/admin/listProducts' />" >Products</a><br>
        <a href="<c:url value='/admin/listPictures/' />" >Pictures</a><br>
        <a href="<c:url value='/admin/listActions/' />" >Actions</a><br>
        <a href="<c:url value='/admin/order/listOrders/' />" >List Orders</a><br>
    </div>

    <div>
        <table border="1">
            <tr>
                <th width="80">Product ID</th>
                <th>Product name</th>
                <th>Product description</th>
                <th>Product price</th>
                <th>Actions for product</th>
                <th>Actions with product</th>
                <th>Images</th>
                <c:if test="${listProducts.size() > 0}">
            </tr>
            <c:forEach items="${listProducts}" var="product">
                <tr>
                    <td>${product.id}</td>
                    <td>${product.nameOfProduct}</td>
                    <td>${product.descriptionOfProduct}</td>
                    <td>${product.price}</td>
                    <td>
                        <c:forEach items="${product.actions}" var="action">
                            ${action.name}
                        </c:forEach>
                    </td>
                    <td><a href="<c:url value='editProduct/${product.id}' />" >Edit</a>
                        <a href="<c:url value='removeProduct/${product.id}' />" >Remove</a>
                    </td>
                    <td>
                        <c:forEach items="${product.picturesProduct}" var="picture">
                            ${picture.pictureId}
                            ${picture.pictureName}

                            <!-- SHOW THE PICTURE IN MINI IMAGE -->
                            <img src="getImagesForProduct/${picture.pictureId}" height="80" width="80"/>
                            <a href="<c:url value='removePicture/${picture.pictureId}' />" >Remove</a>
<%--set name of picture--%>

                        </c:forEach>
                        <br><br>
                        Add picture:
                        <form:form method="POST" action="uploadFile/${product.id}" enctype="multipart/form-data">
                            <table>
                                <tr>
                                    <td>Select pict</td>
                                    <td><input type="file" name="fileUpload" /></td>
                                </tr>
                                <tr>
                                    <td><input type="submit" value="Submit" /></td>
                                </tr>
                            </table>
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
            </c:if>
            <c:if test="${listProducts.size()==0}">
                There are no products
            </c:if>
        </table>
    </div>

    <div><a href="<c:url value='addProduct' />" >Add product</a></div>

</body>
</html>
