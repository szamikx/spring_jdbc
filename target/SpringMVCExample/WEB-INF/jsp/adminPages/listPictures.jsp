<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 16.07.2018
  Time: 10:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of pictures</title>
</head>
<body>
    <div>
        <h2>Categories</h2>
        <br>
        <a href="<c:url value='/admin/listUsers' />" >Users</a><br>
        <a href="<c:url value='/admin/listBaskets' />" >Baskets</a><br>
        <a href="<c:url value='/admin/listProducts' />" >Products</a><br>
        <a href="<c:url value='/admin/listPictures/' />" >Pictures</a><br>
        <a href="<c:url value='/admin/listActions/' />" >Actions</a><br>
        <a href="<c:url value='/admin/order/listOrders/' />" >List Orders</a><br>
    </div>

    <div>
        <table>
            <tr>
                <th width="80">Picture ID</th>
                <th>Picture image</th>
                <th>Picture name</th>
                <th width="120">Product ID</th>
                <th>Actions with picture</th>
                <c:if test="${listPictures.size() > 0}">
            </tr>
                    <c:forEach items="${listPictures}" var="picture">
                        <tr>
                            <td>${picture.pictureId}</td>
                            <td><img src="getImagesForProduct/${picture.pictureId}" height="80" width="80"/></td>

                            <td>${picture.pictureName}</td>
                            <td>${picture.productId.nameOfProduct}</td>
                            <td>
                                <a href="<c:url value='/admin/removePicture/${picture.pictureId}' />" >Delete</a>
                                <a href="<c:url value='/admin/editPicture/${picture.pictureId}' />" >Edit</a>
                            </td>
                        </tr>
                    </c:forEach>
                </c:if>
            <c:if test="${listPictures.size()==0}">
                There are no pictures
            </c:if>
        </table>

    </div>
    <%--<div>--%>
        <%--<br><br>--%>
        <%--Add picture:--%>
        <%--<form:form method="POST" action="uploadFile/${product.id}" enctype="multipart/form-data">--%>
            <%--<table>--%>
                <%--<tr>--%>
                    <%--<td>Select pict</td>--%>
                    <%--<td><input type="file" name="fileUpload" /></td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                    <%--<td><input type="submit" value="Submit" /></td>--%>
                <%--</tr>--%>
            <%--</table>--%>
        <%--</form:form>--%>
        <%----%>
    <%--</div>--%>



</body>
</html>
