<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 08.06.2018
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pictures</title>
</head>
<body>
<div>
    <h2>List of pictures</h2>
    <table>
        <tr>
            <th width="80">Picture ID</th>
            <th width="120">Employee ID</th>
        <c:if test="${pictures.size() > 0}">
            </tr>
                <c:forEach items="${pictures}" var="picture">
                    <tr>
                        <td>${picture.pictureId}</td>
                        <td>${picture.employeeId}</td>
                    </tr>
                </c:forEach>
        </c:if>
        <c:if test="${pictures.size()==0}">
            There are no pictures
        </c:if>
    </table>
</div>
<div>
    <a href="/home/" >Home</a>
</div>
</body>
</html>





