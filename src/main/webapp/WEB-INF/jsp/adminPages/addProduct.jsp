<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 17.07.2018
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adding product</title>
</head>

<body>
<form:form method="POST" modelAttribute="product" action="addProduct">
    <table>
        <tr>
            <td><label for="nameOfProduct">Name: </label> </td>
            <td><form:input path="nameOfProduct" id="nameOfProduct"/></td>
        </tr>

        <tr>
            <td><label for="descriptionOfProduct">Description of Product: </label> </td>
            <td><form:input path="descriptionOfProduct" id="descriptionOfProduct"/></td>
        </tr>

        <tr>
            <td><label for="price">Price: </label> </td>
            <td><form:input path="price" id="price"/></td>
        </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Add"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
<br/>
<br/>

<div>
    Upload pictures of product
    <form:form method="POST"
               action="admin/uploadFile/${id}"
               enctype="multipart/form-data">
        <table>
            <tr>
                <td>Select a file to upload</td>
                <td><input type="file" name="fileUpload" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit" /></td>
            </tr>
        </table>
    </form:form>
</div>





Go back to <a href="<c:url value='admin/listProducts' />">list of All Products</a>

</body>
</html>

