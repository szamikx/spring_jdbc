<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 07.08.2018
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit picture</title>
</head>
<body>
<div>
    Product edit:

    <form:form modelAttribute="picture" method="POST">
        <table>
            <c:if test="${!empty picture.pictureId}">
                <tr>
                    <td> <form:label path="pictureId"> <spring:message text="pictureId"/> </form:label> </td>
                    <td> <form:input path="pictureId" readonly="true" size="8"  disabled="true" /> <form:hidden path="pictureId" /> </td>
                </tr>
            </c:if>
            <tr>
                <td> <form:label path="pictureName"> <spring:message text="Name"/> </form:label> </td>
                <td> <form:input path="pictureName" /> </td>
            </tr>
            <tr>
                <td colspan="2">
                    <c:if test="${!empty picture.pictureId}">
                        <input type="submit" value="<spring:message text="Edit picture"/>" />
                    </c:if>
                </td>
            </tr>
        </table>

    </form:form>
</div>
</body>
</html>
