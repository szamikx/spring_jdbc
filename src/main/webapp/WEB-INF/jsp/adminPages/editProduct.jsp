<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 17.07.2018
  Time: 14:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <div>
        <h2>Categories</h2>
        <a href="<c:url value='/admin/listUsers' />" >Users</a><br>
        <a href="<c:url value='/admin/listBaskets' />" >Baskets</a><br>
        <a href="<c:url value='/admin/listProducts' />" >Products</a><br>
        <a href="<c:url value='/admin/listPictures/' />" >Pictures</a><br>
        <a href="<c:url value='/admin/listActions/' />" >Actions</a><br>
        <a href="<c:url value='/admin/order/listOrders/' />" >List Orders</a><br>
    </div>

    <div>
        Product edit:

        <form:form modelAttribute="product" method="POST">
            <table>
                <c:if test="${!empty product.id}">
                    <tr>
                        <td> <form:label path="id"> <spring:message text="id"/> </form:label> </td>
                        <td> <form:input path="id" readonly="true" size="8"  disabled="true" /> <form:hidden path="id" /> </td>
                    </tr>
                </c:if>
                <tr>
                    <td> <form:label path="nameOfProduct"> <spring:message text="Name"/> </form:label> </td>
                    <td> <form:input path="nameOfProduct" /> </td>
                </tr>
                <tr>
                    <td>  <form:label path="descriptionOfProduct"> <spring:message text="description of Product"/> </form:label> </td>
                    <td>  <form:input path="descriptionOfProduct" /> </td>
                </tr>
                <tr>
                    <td>  <form:label path="price"> <spring:message text="price"/> </form:label> </td>
                    <td>  <form:input path="price" /> </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <c:if test="${!empty product.nameOfProduct}">
                            <input type="submit" value="<spring:message text="Edit product"/>" />
                        </c:if>
                    </td>
                </tr>
            </table>

        </form:form>
    </div>

</body>
</html>
