<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 13.08.2018
  Time: 14:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List Orders</title>
</head>
<body>
    <div>
        <h2>Categories</h2>
        <a href="<c:url value='/admin/listUsers' />" >Users</a><br>
        <a href="<c:url value='/admin/listBaskets' />" >Baskets</a><br>
        <a href="<c:url value='/admin/listProducts' />" >Products</a><br>
        <a href="<c:url value='/admin/listPictures/' />" >Pictures</a><br>
        <a href="<c:url value='/admin/listActions/' />" >Actions</a><br>
        <a href="<c:url value='/admin/order/listOrders/' />" >List Orders</a><br>
    </div>

    <div>
        <table border="1">
            <tr>
                <th width="80">Order ID</th>
                <th>Order date</th>
                <th>Order's user name</th>
                <th>Order's user id</th>
                <th>Items</th>
                <th>Order total amount</th>
                <th>Order status</th>
                <th>Actions with order</th>
                <c:if test="${listOrders.size() > 0}">
            </tr>
            <c:forEach items="${listOrders}" var="order">
                <tr>
                    <td>${order.id}</td>
                    <td>${order.date}</td>
                    <td>${order.user.name}</td>
                    <td>${order.user.id}</td>
                    <td>
                        <c:forEach items="${order.orderItems}" var="item">
                            Line Item Id:  ${item.id}<br>
                            LineItem Prod: ${item.product.nameOfProduct}<br>
                            LineItem Q-ty: ${item.quantity}&nbsp;
                            <%--<br>--%>
                        </c:forEach>

                        <br>
                    </td>
                    <td>${order.totalAmount}</td>
                    <td>${order.orderStatus}</td>
                    <td><a href="<c:url value='/admin/order/removeOrder/${order.id}' />" >Remove order</a>
                        Change order status to:
                        <form:form method="POST" action="/admin/order/changeOrderStatus/${order.id}">
                            <select name="status">
                                <option value="${selected}" selected>${selected}</option>
                                <c:forEach items="${statuses}" var="status">
                                    <c:if test="${status != selected}">
                                        <option value="${status}">${status}</option>
                                    </c:if>
                                </c:forEach>
                            </select>

                            <input type="submit" value="Select">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
            </c:if>
            <c:if test="${listOrders.size()==0}">
                There are no Orders
            </c:if>
        </table>
    </div>

</body>
</html>
