<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Bagama
  Date: 21.07.2018
  Time: 23:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add user</title>
</head>
<body>
    <form:form method="POST" modelAttribute="user" action="addUser">
        <table>
        <tr>
            <td><label for="name">Name: </label> </td>
            <td><form:input path="name" id="name"/></td>
        </tr>

        <tr>
            <td><label for="role">Role: </label> </td>
            <td><form:input path="role" id="role"/></td>
        </tr>

        <tr>
            <td><label for="status">Status: </label> </td>
            <td><form:input path="status" id="status"/></td>
        </tr>

        <tr>
            <td><label for="email">email: </label> </td>
            <td><form:input path="email" id="email"/></td>
        </tr>

        <tr>
            <td><label for="password">password: </label> </td>
            <td><form:input path="password" id="password"/></td>
        </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Add"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
        </table>
    </form:form>
    </body>
</html>
