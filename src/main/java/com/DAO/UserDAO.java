package com.DAO;

import com.Model.User;

import java.util.List;

public interface UserDAO {
    User getUserById(Long userId);
    User getUserByName(String name);
    List<User> getListOfUsers();
    boolean updateUser(User user);
    boolean removeUser(User user);
    boolean saveUser(User user);
}
