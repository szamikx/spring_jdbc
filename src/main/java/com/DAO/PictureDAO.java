package com.DAO;

import com.Model.Picture;

import java.util.List;

public interface PictureDAO {
    void insertPicture(Picture picture);
    void removePicture(Picture picture);
    Picture getPictureById(Long id);
    void updatePicture(Picture picture);
    List<Picture> listPicture();
    List<Picture> getPictureByProductId(Long id);
}
