package com.DAO;

import com.Model.Product;

import java.util.List;

public interface ProductDAO {
    Product getProductById(Long idProduct);
    Product getProductByName(String nameOfProduct);
    List<Product> getListOfProducts();
    boolean updateProduct(Product product);
    boolean removeProduct(Product product);
    boolean saveProduct(Product product);
}
