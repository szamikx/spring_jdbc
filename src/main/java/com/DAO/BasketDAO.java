package com.DAO;

import com.Model.Basket;
import com.Model.User;

import java.util.List;

public interface BasketDAO {
    Basket getBasketByUser(User user);
    Basket getBasketByID(Long basketId);
    boolean updateBasket(Basket basket);
    boolean removeBasket(Basket basket);
    Basket saveBasket(Basket basket);
    List<Basket> getBasketList();
}
