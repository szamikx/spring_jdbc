package com.DAO;

import com.Model.LineItem;

import java.util.List;

public interface LineItemDAO {
    LineItem getById(Long id);
    List<LineItem> getbyBasketId(Long id);
    void saveLineItem(LineItem lineItem);
    void updateLineItem(LineItem lineItem);
    boolean removeLineItem(LineItem lineItem);
    LineItem getbyBasketIdAndProductName(Long id, String productName);
}
