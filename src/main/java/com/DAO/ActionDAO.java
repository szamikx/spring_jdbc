package com.DAO;

import com.Model.Action;
import com.Model.Product;

import java.util.List;

public interface ActionDAO {

    void deleteAction(Action action);
    Action saveAction(Action action);
    Action editAction(Action action);
    List<Action> listActions();
    Action getActionById(Long id);
    Action getActionByName(String name);
    void addProductToAction(Action action, Product product);
    void removeProductFromAction(Action action, Product product);
}
