package com.DAO.DAOImplementation;

import com.DAO.LineItemDAO;
import com.Model.LineItem;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

public class LineItemDAOImpl implements LineItemDAO {

    @Autowired
    SessionFactory sessionFactory;

    Logger logger = Logger.getLogger("main");

    @Override
    @Transactional(readOnly = true)
    public LineItem getById(Long id) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Query query = session.createQuery("SELECT lineItem FROM LineItem lineItem " +
                "LEFT JOIN FETCH lineItem.basket " +
                "LEFT JOIN FETCH lineItem.product " +
                "LEFT JOIN FETCH lineItem.order " +
                "WHERE lineItem.id=:id");
        query.setParameter("id", id);
        LineItem lineItem = (LineItem) query.uniqueResult();
        logger.info("LINEITEMDAOIMPL - GETBYID - got LineItem : " + lineItem);
        return lineItem;
    }

    @Override
    @Transactional(readOnly = true)
    public List<LineItem> getbyBasketId(Long id) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Query query = session.createQuery("SELECT DISTINCT lineItem FROM LineItem lineItem");
        List<LineItem> lineItemList = query.list();
        logger.info("LINEITEMDAOIMPL - UPDATELINEITEM - Get list of lineItem by basketId: " + lineItemList);
        return lineItemList;
    }

    @Override
    @Transactional
    public void saveLineItem(LineItem lineItem) {
        Session session = sessionFactory.openSession();
        session.save(lineItem);
        session.flush();
        if(lineItem.getId() != 0)
            logger.info("LINEITEMDAOIMPL - UPDATELINEITEM - Line item saved: " + lineItem);
        else logger.info("LINEITEMDAOIMPL - UPDATELINEITEM - Line item is not saved: " + lineItem);
        session.close();
    }

    @Override
    @Transactional
    public void updateLineItem(LineItem lineItem) {
        logger.info("LINEITEMDAOIMPL - UPDATELINEITEM - get line item: " + lineItem);
        Session session = sessionFactory.getCurrentSession();
        int quantity = lineItem.getQuantity();

        Query query = session.createQuery("UPDATE LineItem lineItem SET " +
//                "lineItem.basket=:" + lineItem.getBasket() +
//                ", lineItem.order =: " + lineItem.getOrder() +
//                ", lineItem.product=:" + lineItem.getProduct() +
                "lineItem.quantity=:quantity" +
                " WHERE lineItem.id=" + lineItem.getId());
        query.setParameter("quantity", quantity);
        query.executeUpdate();
//        session.merge(lineItem);
        logger.info("LINEITEMDAOIMPL - UPDATELINEITEM - lineItem after updating: " + this.getById(lineItem.getId()));
//        session.merge(liFromDB);

        session.flush();
    }

    @Override
    @Transactional
    public boolean removeLineItem(LineItem lineItem) {
        long lineItemId = lineItem.getId();
        logger.info("**************** - id переданное - " + lineItemId);
        Session session = sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery("DELETE FROM lineitems WHERE id=:id");
        query.setParameter("id", lineItemId);
        logger.info("*********LINEITEMDAO - REMOVE LINE ITEM***********" + query.toString());
        query.executeUpdate();
        session.flush();
        logger.info("LINEITEMDAOIMPL - DeleteLINEITEM - ***********LineItem is deleted:" + lineItem);
        return true;
    }

    @Override
    @Transactional
    public LineItem getbyBasketIdAndProductName(Long id, String productName) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        List<LineItem> lineItemList = this.getbyBasketId(id);
        for(LineItem lineItem: lineItemList){
            if(lineItem.getProduct().getNameOfProduct().equals(productName) ){
                logger.info("LINEITEMDAOIMPL - UPDATELINEITEM - Line Item is found by basketId and byName of product:" + lineItem);
                return lineItem;
            }
        }
        return null;
    }
}
