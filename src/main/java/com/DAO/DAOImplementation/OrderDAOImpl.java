package com.DAO.DAOImplementation;

import com.DAO.LineItemDAO;
import com.DAO.OrderDAO;
import com.Model.LineItem;
import com.Model.Order;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

public class OrderDAOImpl implements OrderDAO {
    Logger logger = Logger.getLogger("main");

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    LineItemDAO lineItemDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Order> listOrders() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT order FROM Order order");// +
//                "LEFT JOIN FETCH order.orderItems");
        List<Order> orderList = query.list();
        session.flush();
        if(orderList != null) {
            logger.info("OrderDAO - orderList - list:" + orderList);
            return orderList;
        } else
            return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Order getOrderById(Long orderId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT order FROM Order order WHERE order.id=:id");
        query.setParameter("id", orderId);
        Order order = (Order) query.uniqueResult();
        session.flush();

        if(order != null){
            logger.info("OrderDAO - getOrderById - order is found:" + order);
            return order;
        } else {
            logger.info("OrderDAO - getOrderById - order is not found..");
            return null;
        }
    }

    @Override
    @Transactional
    public void saveOrder(Order order) {
        Session session = sessionFactory.getCurrentSession();
        session.save(order);
        session.flush();
        logger.info("OrderDAO - saveOrder - order is saved, id is:" + order.getId());

        List<LineItem> lineItems = order.getOrderItems();
        logger.info("OrderDAO - saveOrder - order is saved, lineItems are:" + order.getOrderItems());
    }

    @Override
    @Transactional
    public void updateOrder(Order order) {
        Session session = sessionFactory.getCurrentSession();
        Order order1 = this.getOrderById(order.getId());
        order1.setDate(order.getDate());
        order1.setOrderStatus(order.getOrderStatus());
        order1.setTotalAmount(order.getTotalAmount());
        order1.setUser(order.getUser());
        order1.setOrderItems(order.getOrderItems());

//        session.merge(order1);
        session.merge(order);
        session.flush();
        logger.info("OrderDAO - updateOrder - order is updated: " + order1);

    }

    @Override
    @Transactional
    public void deleteOrder(Long orderId) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(this.getOrderById(orderId));
        session.flush();
        logger.info("OrderDAO - deleteOrder - order is deleted:" + this.getOrderById(orderId));
    }
}
