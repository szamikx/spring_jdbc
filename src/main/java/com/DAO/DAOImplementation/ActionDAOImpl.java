package com.DAO.DAOImplementation;

import com.DAO.ActionDAO;
import com.Model.Action;
import com.Model.Product;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class ActionDAOImpl implements ActionDAO {

    @Autowired
    SessionFactory sessionFactory;

    Logger logger = Logger.getLogger("main");

    @Override
    @Transactional
    public void deleteAction(Action action) {
        Session session = sessionFactory.openSession();
        session.delete(action);
        session.flush();
        logger.info("Action is deleted: " + action);
    }

    @Override
    @Transactional
    public Action saveAction(Action action) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        session.save(action);
        logger.info("Action is saved: " + action);
        return action;
    }

    @Override
    @Transactional
    public Action editAction(Action action) {
        logger.info("inside editAction.... action:" + action);
        Session session = sessionFactory.getCurrentSession();//openSession();
        session.update(action);
        session.flush();
        logger.info("Action is updated:" + action);
        return action;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Action> listActions() {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Query query = session.createQuery("SELECT action FROM Action action");
        List<Action> actionList = query.list();
        logger.info("Action list is loaded:" + actionList);
        return actionList;
    }

    @Override
    @Transactional(readOnly = true)
    public Action getActionById(Long id) {
        logger.info("inside get actionById, id is:" + id);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT action FROM Action action WHERE action.id =:id");
        query.setParameter("id", id);
        Action action = (Action) query.uniqueResult();
        logger.info("Get Action from DB: " + action);
        return action;
    }

    @Override
    @Transactional(readOnly = true)
    public Action getActionByName(String name) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Query query = session.createQuery("SELECT action FROM Action action WHERE action.name=:name");
        query.setParameter("name", name);
        Action action = (Action) query.uniqueResult();
        logger.info("Got action by name" + name + " . Action: " + action);
        return action;
    }

    @Override
    @Transactional
    public void addProductToAction(Action action, Product product) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Action actionTemp = this.getActionById(action.getId());
        Set<Product> productSet = actionTemp.getProducts();
        productSet.add(product);
        this.editAction(actionTemp);
        session.flush();
        logger.info("ActionDAOIMpl... addProductToAction.. action updated" + actionTemp);
    }

    @Override
    @Transactional
    public void removeProductFromAction(Action action, Product product) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Action actionTemp = this.getActionById(action.getId());
        Set<Product> productSet = actionTemp.getProducts();
        product.removeAction(action);
        Query query = session.createSQLQuery("DELETE FROM productsactions WHERE idproduct=:idProduct AND idaction=:idAction");
        query.setParameter("idProduct", product.getId());
        query.setParameter("idAction", actionTemp.getId());
        query.executeUpdate();
        logger.info("IS PRODUCT REMOVED: " + productSet.remove(product));
        logger.info("INSIDE REMOVEPRODUCTFROMACTION DAO PRODUCTS AFTER REMOVING:" + productSet);
        this.editAction(actionTemp);
        session.flush();
        logger.info("ActionDAOIMpl... removeProductFromAction.. action updated" + actionTemp.getProducts());
    }


}
