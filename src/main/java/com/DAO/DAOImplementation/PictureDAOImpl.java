package com.DAO.DAOImplementation;

import com.DAO.PictureDAO;
import com.Model.Picture;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;


@CacheConfig(cacheNames = {"appCache"})
public class PictureDAOImpl implements PictureDAO {

    @Autowired
    SessionFactory sessionFactory;

    Logger logger = Logger.getLogger("main");

    @CachePut
    @Override
    @Transactional
    public void insertPicture(Picture picture) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        logger.info("Inside pcitureDAO - insertPicture..");
        session.save(picture);
        session.flush();
        logger.info("picture is saved, picture id is " + picture);

    }

    @CacheEvict
    @Override
    @Transactional
    public void removePicture(Picture picture) {
        Session session = sessionFactory.openSession();
        session.delete(picture);
        session.flush();
        logger.info("picture is deleted, picture id is " + picture.getPictureId());

    }

//    @Cacheable
    @Override
    @Transactional(readOnly = true)
    public Picture getPictureById(Long id) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT picture FROM Picture picture " +
                "LEFT JOIN FETCH picture.productId " +
                "WHERE picture.pictureId=:id");
        query.setParameter("id", id);
        Picture picture = (Picture) query.uniqueResult();
        if(picture != null) {
            logger.info("picture is found, picture id is " + picture);
            return picture;
        } else {
            logger.info("picture is not found");
            return null;
        }
    }

//    @Cacheable
    @Override
    @Transactional
    public void updatePicture(Picture picture) {
        logger.info("Inside PictureDAOImpl - updatePicture: " + picture);
        Picture picture1 = this.getPictureById(picture.getPictureId());
        picture1.setPictureName(picture.getPictureName());
        Session session = sessionFactory.openSession();
        session.saveOrUpdate(picture1);
        session.flush();
        logger.info("picture is updated.., picture id is " + picture1.getPictureId());
    }

//    @Cacheable
    @Override
    @Transactional(readOnly = true)
    public List<Picture> listPicture() {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Query query = session.createQuery("SELECT picture FROM Picture picture");
        List<Picture> list = query.list();
        session.flush();
        logger.info("list of pictures from listPicture() from DAO:" + list);
        return list;
    }

    @Override
    public List<Picture> getPictureByProductId(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT picture FROM Picture picture " +
                "LEFT JOIN FETCH picture.productId " +
                "WHERE picture.productId=:id");
        query.setParameter("id", id);
        List<Picture> pictureList = query.list();
        session.flush();
        logger.info("getting list of pict5ure by product id.. " + pictureList);
        return pictureList;
    }
}
