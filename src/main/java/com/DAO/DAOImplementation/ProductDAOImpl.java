package com.DAO.DAOImplementation;

import com.DAO.ProductDAO;
import com.Model.Product;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

public class ProductDAOImpl implements ProductDAO {

    @Autowired
    SessionFactory sessionFactory;

    Logger logger = Logger.getLogger("main");


    @Override
    @Transactional(readOnly = true)
    public Product getProductById(Long idProduct) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT product FROM Product product " +
//                "LEFT JOIN FETCH product.picturesProduct " +
                "LEFT JOIN FETCH product.actions " +
                " WHERE product.id=:id");
        query.setParameter("id", idProduct);
        Product product = (Product) query.uniqueResult();
        logger.info("found product with id:" + product);
        session.close();
        return product;
    }

    @Override
    @Transactional(readOnly = true)
    public Product getProductByName(String nameOfProduct) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT product from Product product " +
//                "LEFT JOIN FETCH product.picturesProduct " +
                "LEFT JOIN FETCH product.actions " +
                "WHERE product.nameOfProduct=:name");
        query.setParameter("name", nameOfProduct);
        Product product = (Product) query.uniqueResult();
        logger.info("found product by name:" + product);
//        session.close();
        return product;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> getListOfProducts() {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT product FROM Product product");
        List<Product> productList = query.list();
        logger.info("list of products:" + productList);
        return productList;
    }

    @Override
    @Transactional
    public boolean updateProduct(Product product) {
        Session session = sessionFactory.openSession();
        session.update(product);
        session.flush();
        logger.info("product updated:" + product);
        return true;
    }

    @Override
    @Transactional
    public boolean removeProduct(Product product) {
        logger.info("Inside ProductDAOIMpl - removeProduct.. ");
        Session session = sessionFactory.getCurrentSession();
        session.delete(product);
        session.flush();
        if(session.get(Product.class, product.getId()) == null){
            logger.info("Inside ProductDAOIMpl.. product is removed:" + product);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean saveProduct(Product product) {
        Session session = sessionFactory.openSession();
        session.save(product);
        if(product.getId() != 0) {
            return true;
        }
        return false;
    }
}
