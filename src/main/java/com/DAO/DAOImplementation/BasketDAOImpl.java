package com.DAO.DAOImplementation;

import com.DAO.BasketDAO;
import com.DAO.LineItemDAO;
import com.Model.Basket;
import com.Model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

public class BasketDAOImpl implements BasketDAO {

    @Autowired
    SessionFactory sessionFactory;

    Logger logger = Logger.getLogger("main");

    @Autowired
    LineItemDAO lineItemDAO;

    @Override
    @Transactional(readOnly = true)
    public Basket getBasketByUser(User user){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT basket FROM Basket basket " +
                "LEFT JOIN FETCH basket.user " +
//                "LEFT JOIN FETCH basket.lineItems " +
                "WHERE basket.user.id=:userId");
        query.setParameter("userId", user.getId());
        Basket basket = (Basket) query.uniqueResult();
        logger.info("found basket by user:" + basket);
        return basket;
    }

    @Override
    @Transactional(readOnly = true)
    public Basket getBasketByID(Long basketId) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        Query query = session.createQuery("SELECT DISTINCT basket FROM Basket basket " +
                "LEFT JOIN FETCH basket.user " +
//                "LEFT JOIN FETCH basket.lineItems " +
                "WHERE basket.id=:basketId");
        query.setParameter("basketId", basketId);
        Basket basket = (Basket) query.uniqueResult();//session.get(Basket.class, basketId);
        logger.info("found basket by id: " + basket);
        return basket;
    }

    @Override
    @Transactional
    public boolean updateBasket(Basket basket) {
        Session session = sessionFactory.openSession();
        session.merge(basket);
        session.flush();
//        if(session.merge(basket) != null){
//            logger.info("basket merged:" + basket);
            return true;
//        }
//        return false;
    }

    @Override
    @Transactional
    public boolean removeBasket(Basket basket) {
        logger.info("inside removeBasket....");
        Session session = sessionFactory.getCurrentSession();//openSession();
        session.delete(basket);
            logger.info("removed basket:" + basket);
        return true;
    }

    @Override
    @Transactional
    public Basket saveBasket(Basket basket) {
        Session session = sessionFactory.openSession();
        session.saveOrUpdate(basket);
        logger.info("basket is saved with id :" + basket.getId());
        return basket;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Basket> getBasketList() {
//        logger.info("inside getBasketist DAOIMpl..");
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT basket FROM Basket basket");
        List<Basket> basketList = query.list();
//        logger.info("list of baskets:" + basketList);
        return basketList;
    }
}
