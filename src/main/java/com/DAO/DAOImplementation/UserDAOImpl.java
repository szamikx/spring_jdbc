package com.DAO.DAOImplementation;

import com.DAO.BasketDAO;
import com.DAO.UserDAO;
import com.Model.Basket;
import com.Model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

public class UserDAOImpl implements UserDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    BasketDAO basketDAO;

    Logger logger = Logger.getLogger("main");

    @Override
    @Transactional(readOnly = true)
    public User getUserById(Long userId) {
        logger.info("inside UserDAO... get userbyId...");
        Session session = sessionFactory.getCurrentSession();//openSession();
        Query userQuery = session.createQuery("SELECT user FROM User user " +
                "WHERE user.id=:id");
        userQuery.setParameter("id", userId);
        User user = (User) userQuery.uniqueResult();
        logger.info("found user with id:" + user);
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public User getUserByName(String name) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT user from User user" +
                "WHERE user.name=:name");
        query.setParameter("name", name);
        User user = (User) query.uniqueResult();
        logger.info("found user by name:" + user);
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getListOfUsers() {
        logger.info("Inside UserDAOImpl getListOfUsers()........");
        Session session = sessionFactory.openSession();//getCurrentSession();
        try {
            Query query = session.createQuery("SELECT user FROM User user");
            List<User> userList = query.list();
//            logger.info("get users list:" + userList);
            return userList;
        }catch (Exception e){
            System.out.println("EXCEPTION INSIDE USERDAOIMPL getListOfUsers()...");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public boolean updateUser(User user) {
        Session session = sessionFactory.openSession();
        session.update(user);
        session.flush();
        return true;
    }

    @Override
    @Transactional
    public boolean removeUser(User user) {
        Session session = sessionFactory.getCurrentSession();//openSession();
        session.delete(user);
            logger.info("have removed user:" + user);
            return true;
    }

    @Override
    @Transactional
    public boolean saveUser(User user) {
        Session session = sessionFactory.openSession();
        session.save(user);
        if(user.getId() != 0) {
            logger.info("user have been saved, userId:" + user.getId());
            Basket newBasket = new Basket();
//            newBasket.setOrderStatus(OrderStatus.RESERVED);
            newBasket.setUser(user);
            basketDAO.saveBasket(newBasket);
            return true;
        }
        return false;
    }
}

//        int idUser = user.getId();
//        logger.info("userId is ...." + idUser);
//        Query query = session.createQuery("SELECT basket from Basket basket WHERE basket.user.id=:id");
//        query.setParameter("id", idUser);
//        Basket basket = (Basket) query.uniqueResult();
//        logger.info("get basket: " + basket);
//        session.delete(basket);