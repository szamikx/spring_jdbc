package com.DAO;

import com.Model.Order;
import java.util.List;

public interface OrderDAO {
    List<Order> listOrders();
    Order getOrderById(Long orderId);
    void saveOrder(Order order);
    void updateOrder(Order order);
    void deleteOrder(Long orderId);
}
