use employeesdb;

INSERT INTO products(id, nameOfProduct, descriptionOfProduct, price) VALUES (1,'Product1', 'Description of product1', 12.1);
INSERT INTO products(id, nameOfProduct, descriptionOfProduct, inAction, price) VALUES (2, 'Product2', 'Description of product2', TRUE, 11.9 );
INSERT INTO products(id, nameOfProduct, descriptionOfProduct, inAction, price) VALUES (3, 'Product3', 'Description of product3', FALSE, 7.9 );
INSERT INTO products(id, nameOfProduct, descriptionOfProduct, inAction, price) VALUES (4, 'Product4', 'Description of product4', TRUE, 8.8 );
INSERT INTO products(id, nameOfProduct, descriptionOfProduct, inAction, price) VALUES (5, 'Product5', 'Description of product5', FALSE, 56.1 );

INSERT INTO users(id, name, role, userstatus, email) VALUES (1, 'User1', 'Admin', 'Active', 'sss@gmail.com');
INSERT INTO users(id, name, role, userstatus, email) VALUES (2, 'User2', 'Manager', 'Active', 'sss2@gmail.com');
INSERT INTO users(id, name, role, userstatus, email) VALUES (3, 'User3', 'User', 'Active', 'sss3@gmail.com');
INSERT INTO users(id, name, role, userstatus, email) VALUES (4, 'User4', 'Guest', 'Active', 'sss4@gmail.com');

INSERT INTO baskets(userId) VALUES (1);
INSERT INTO baskets(userId) VALUES (2);
INSERT INTO baskets(userId) VALUES (3);
INSERT INTO baskets(userId) VALUES (4);


INSERT INTO lineitems(basketId, productId, quantity) VALUES (4, 1, 3);
INSERT INTO lineitems(basketId, productId, quantity) VALUES (1, 3, 1);
INSERT INTO lineitems(basketId, productId, quantity) VALUES (4, 2, 1);
INSERT INTO lineitems(basketId, productId, quantity) VALUES (2, 4, 12);
INSERT INTO lineitems(basketId, productId, quantity) VALUES (3, 5, 3);
INSERT INTO lineitems(basketId, productId, quantity) VALUES (3, 2, 1);

# INSERT INTO Baskets_LineItems(basketId, lineItemId) VALUES (4, 1);
# INSERT INTO Baskets_LineItems(basketId, lineItemId) VALUES (1, 2);
# INSERT INTO Baskets_LineItems(basketId, lineItemId) VALUES (4, 3);
# INSERT INTO Baskets_LineItems(basketId, lineItemId) VALUES (2, 4);
# INSERT INTO Baskets_LineItems(basketId, lineItemId) VALUES (3, 5);
# INSERT INTO Baskets_LineItems(basketId, lineItemId) VALUES (3, 6);






INSERT INTO actions(name, description, quantity, discountPercent) VALUES ('action1', 'action1 description', 10, 1.2);
INSERT INTO actions(name, description, quantity, discountPercent) VALUES ('action1', 'action1 description', 15, 2.2);
INSERT INTO actions(name, description, quantity, discountPercent) VALUES ('action1', 'action1 description', 20, 3.2);

INSERT INTO productsactions(idProduct, idAction) VALUES (1, 1);
INSERT INTO productsactions(idProduct, idAction) VALUES (1, 2);
INSERT INTO productsactions(idProduct, idAction) VALUES (1, 3);
INSERT INTO productsactions(idProduct, idAction) VALUES (2, 2);
INSERT INTO productsactions(idProduct, idAction) VALUES (2, 3);