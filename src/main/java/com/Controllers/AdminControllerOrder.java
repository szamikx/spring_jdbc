package com.Controllers;


import com.Model.Basket;
import com.Model.Order;
import com.Model.OrderStatus;
import com.Service.BasketService;
import com.Service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping("/admin/order")
public class AdminControllerOrder {

    @Autowired
    OrderService orderService;

    @Autowired
    BasketService basketService;

    Logger logger = Logger.getLogger("main");

    @RequestMapping(path = "/listOrders", method = RequestMethod.GET)
    public String getListOrders(Model model){
        List<Order> listOrders = orderService.getOrderList();
        List<OrderStatus> orderStatusList = Arrays.asList(OrderStatus.values());
//        logger.info("AdminController - list of orders:" + listOrders);
        model.addAttribute("listOrders", listOrders);
        model.addAttribute("statuses", orderStatusList);
        return "/adminPages/listOfOrders";
    }

    @RequestMapping(path = "/removeOrder/{id}", method = RequestMethod.GET)
    public String removeOrder(Model model, @PathVariable("id") Long orderId){
        orderService.deleteOrder(orderId);
        logger.info("AdminController - remove order:" + orderId);
        return "redirect:/admin/order/listOrders";
    }

    @RequestMapping(path = "changeOrderStatus/{orderId}", method = RequestMethod.POST)
    public String changeOrderStatus(@RequestParam("status") String status, @PathVariable("orderId") Long orderId){
        Order order = orderService.getOrderById(orderId);
        order.setOrderStatus(OrderStatus.valueOf(status));
        orderService.updateOrder(order);
        logger.info("AdminController - changeOrderStatus is changed:" + order);
        return "redirect:/admin/order/listOrders";
    }

    @RequestMapping(path = "/makeOrderFromBasket/{id}", method = RequestMethod.GET)
    public String makeOrderFromBasket(Model model, @PathVariable("id") Long basketId){
        Basket basket = basketService.getBasketByID(basketId);
        Order order = orderService.createOrderFromBasket(basket);
//        basketService.cleanBasket(basketId);
        logger.info("AdminController - create order from basket, order created:" + order);
        return "redirect:/admin/order/listOrders";
    }

}
