package com.Controllers;

import com.DAO.LineItemDAO;
import com.Model.*;
import com.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;


@Controller
@RequestMapping("/admin/")
public class adminController {

    Logger logger = Logger.getLogger("main");

    @Autowired
    BasketService basketService;

    @Autowired
    UserService userService;

    @Autowired
    LineItemDAO lineItemDAO;

    @Autowired
    ActionService actionService;

    @Autowired
    PictureService pictureService;

    @Autowired
    ProductService productService;

    @Autowired
    OrderService orderService;

    @Autowired
    LineItemService lineItemService;

    @RequestMapping(path = "adminHome")
    public String adminHome(){
        return "adminPages/adminHome";
    }

    @RequestMapping(path = "/listUsers", method = RequestMethod.GET)
    public String getListUsers(Model model){
        List<User> listUsers = userService.listUsers();
        logger.info("list of users from DAO:" + listUsers);
        model.addAttribute("listUsers", listUsers);
        return "adminPages/listUsers";
    }

    @RequestMapping(path = "/listBaskets", method = RequestMethod.GET)
    public String getListBaskets(Model model){
        List<OrderStatus> orderStatusList = Arrays.asList(OrderStatus.values());
        List<Basket> listBaskets = basketService.listBaskets();
        List<Product> listProducts = productService.listProducts();
        logger.info("list of baskets from DAO:" + listBaskets);
        model.addAttribute("listBaskets", listBaskets);
        model.addAttribute("products" , listProducts);
        model.addAttribute("statuses", orderStatusList);
        return "adminPages/listBaskets";
    }

    @RequestMapping(path = "/listProducts", method = RequestMethod.GET)
    public String getListProducts(Model model){
        Picture picture = new Picture();
        List<Product> listProducts = productService.listProducts();//productDAO.getListOfProducts();
        logger.info("list of products from DAO:" + listProducts);
        model.addAttribute("listProducts", listProducts);
        model.addAttribute("picture", picture);
        return "adminPages/productList";
    }

    @RequestMapping(path = "/listPictures", method = RequestMethod.GET)
    public String getListPictures(Model model){
        Picture picture = new Picture();
        List<Picture> listPictures = pictureService.listPictures();
        logger.info("list of pictures from DAO:" + listPictures);
        model.addAttribute("listPictures", listPictures);
        model.addAttribute("picture", picture);
        logger.info("model as Map from AdminController: " + model.asMap());
        return "/adminPages/listPictures";
    }

    @RequestMapping(path = "/editPicture/{id}", method = RequestMethod.GET)
    public String editPicture(Model model, @PathVariable("id") Long id){
        Picture picture = pictureService.getPictureById(id);
        model.addAttribute("picture", picture);
        return "/adminPages/editPicture";
    }

    @RequestMapping(path = "/editPicture/{id}", method = RequestMethod.POST)
    public String editPicture(Model model, Picture picture){
        logger.info("Admin controller - edit Picture: " + picture);
        pictureService.updatePicture(picture);
        return "redirect:/admin/listPictures";
    }

    @ResponseBody
    @RequestMapping(path = "getImagesForProduct/{pictureId}", method = RequestMethod.GET)
    public byte[] getImageAsByteArray(@PathVariable("pictureId") Long pictureId){
        byte[] pictureData = pictureService.getImagesForProduct(pictureId);
        return pictureData;
    }

    @RequestMapping(path = "/removePicture/{id}", method = RequestMethod.GET)
    public String removePicture(Model model, @PathVariable("id") Long id){
        pictureService.removePicture(id);
        logger.info("AdminController - picture is removed, id:" + id);
        return "redirect:/admin/listProducts";
    }


    @RequestMapping(path = "/listActions", method = RequestMethod.GET)
    public String getListActions(Model model){
        List<Action> listActions = actionService.listActions();
        Action action = new Action();
        List<Product> listProducts = productService.listProducts();
        logger.info("list of actions from DAO:" + listActions);
        model.addAttribute("listActions", listActions);
        model.addAttribute("products" , listProducts);
        model.addAttribute("action1", action);
        logger.info("Model as map from listActions:" + model.asMap());
        return "adminPages/listActionsPage";
    }

    @RequestMapping(path = "/addProductToAction/{id}", method = RequestMethod.POST)
    public String addProductToAction(Model model, @PathVariable("id") Long id, @RequestParam("product") String nameOfProduct){
        actionService.addProductToAction(id, nameOfProduct);
        logger.info("inside AdminController... addProductToAction...");
        return "redirect:/admin/listActions";
    }

    @RequestMapping(path = "/removeProductFromAction/{id}/{productId}", method = RequestMethod.GET)
    public String removeProductFromAction(Model model, @PathVariable("id") Long id, @PathVariable("productId") Long productId){
        actionService.removeProductFromAction(id, productId);
        return "redirect:/admin/listActions";
    }


    @RequestMapping(path = "/removeUser/{id}", method = RequestMethod.GET)
    public String removeUser(Model model, @PathVariable("id") Long id){
        logger.info("inside adminController... removeUser...");
        userService.removeUser(id);
        logger.info("User with id: " + id + "is removed.");
        return "redirect:/admin/listUsers";
    }

    @RequestMapping(path = "deactivateUser/{id}", method = RequestMethod.GET)
    public String deactivateUser(Model model, @PathVariable("id") Long id){
        User user = userService.getUserById(id);
        user.setStatus(UserStatus.NonActive);
        logger.info("after setting userStatus to inactive..." + user);
        userService.updateUser(user);

        logger.info("User status set to inactive. User id:" + id);
        return "redirect:/admin/listUsers";
    }

    @RequestMapping(path = "activateUser/{id}", method = RequestMethod.GET)
    public String activateUser(Model model, @PathVariable("id") Long id){
        User user = userService.getUserById(id);
        user.setStatus(UserStatus.Active);
        logger.info("after setting userStatus to inactive..." + user);
        userService.updateUser(user);

        logger.info("User status set to active. User id:" + id);
        return "redirect:/admin/listUsers";
    }

    @RequestMapping(path = "changeRole/{id}", method = RequestMethod.POST)
    public String changeRoleUser(@RequestParam("roles") String roles, Model model, @PathVariable("id") Long id){
        User user = userService.getUserById(id);
        logger.info("requestParam from request:" + roles);
        logger.info("model from request:" + model.asMap());

        if(roles.equals("Admin")) {
            user.setRole(Role.Admin);
        }else if(roles.equals("Manager")){
            user.setRole(Role.Manager);
        }else if(roles.equals("User")){
            user.setRole(Role.User);
        }else if(roles.equals("Guest")){
            user.setRole(Role.Guest);
        }
        userService.updateUser(user);
        logger.info("User role is set to " + roles + "user Id is:" + id);
        return "redirect:/admin/listUsers";
    }

    @RequestMapping(path = "/addUser", method = RequestMethod.GET)
    public String addUser(Model model){
        User user = new User();
        model.addAttribute("user", user);
        return "adminPages/addUser";
    }

    @RequestMapping(path = "/addUser", method = RequestMethod.POST)
    public String addUser(User user){
        userService.addUser(user);
        logger.info("User is saved: " + user);
        return "redirect:/admin/listUsers";
    }

    @RequestMapping(path = "/addProduct", method = RequestMethod.GET)
    public String addProduct(Model model){
        Product product = new Product();
        model.addAttribute("product", product);
        return "adminPages/addProduct";
    }

    @RequestMapping(path = "/addProduct", method = RequestMethod.POST)
    public String addProduct(Product product){
        productService.addProduct(product);
        logger.info("Product is saved: " + product);
        return "redirect:listProducts";
    }

    @RequestMapping(path = "/editProduct/{id}", method = RequestMethod.GET)
    public String editProduct(Model model, @PathVariable("id") Long id){
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        return "adminPages/editProduct";
    }

    @RequestMapping(path = "/editProduct/{id}", method = RequestMethod.POST)
    public String editProduct(Product product, @PathVariable("id") Long id){
        productService.updateProduct(product);
        logger.info("Product is updated: " + product);
        return "redirect:/admin/listProducts";
    }

    @RequestMapping(path = "/removeProduct/{id}", method = RequestMethod.GET)
    public String removeProduct(Model model, @PathVariable("id") Long id){
        if(id != 0) {
            productService.removeProduct(id);
        }
        return "redirect:/admin/listProducts";// admin/listProducts";
    }

    @RequestMapping(path = "/editBasket/{id}", method = RequestMethod.GET)
    public String editBasket(@PathVariable("id") Long id){
        basketService.removeBasket(id);
        logger.info("Basket with id " + id + "is removed : " + basketService.getBasketByID(id));
        return "redirect: admin/listBaskets";
    }

    @RequestMapping(path = "removeAction/{id}", method = RequestMethod.GET)
    public String removeAction(@PathVariable("id") Long id){
        actionService.removeAction(id);
        return "redirect:/admin/listActions";
    }

    @RequestMapping(path = "editAction/{id}", method = RequestMethod.GET)
    public String editAction(Model model, @PathVariable("id") Long id){
        Action action = actionService.getActionById(id);
        model.addAttribute("action", action);
        return "adminPages/editAction";
    }

    @RequestMapping(path = "editAction/", method = RequestMethod.POST)
    public String editAction(Action action){
        actionService.editAction(action);
        return "redirect:/admin/listActions";
    }

    @RequestMapping(path = "addAction/", method = RequestMethod.POST)
    public String addAction(Model model, Action action){
        actionService.addAction(action);
        return "redirect:/admin/listActions";
    }

    @RequestMapping(path = "/removeBasket/{id}", method = RequestMethod.GET)
    public String removeBasket(Model model, @PathVariable("id") Long id){
        logger.info("inside adminController... removeBasket...");
        basketService.removeBasket(id);
        logger.info("Basket with id: " + id + "is removed.");
        return "redirect:/admin/listBaskets";
    }

    @RequestMapping(path = "/addItemToBasket/{id}", method = RequestMethod.POST)
    public String addItemToBasket(@RequestParam("product") String nameOfProduct, Model model, @PathVariable("id") Long id){
        Basket basket = basketService.getBasketByID(id);
        List<LineItem> lineItems = basket.getLineItems();
        LineItem lineItem = new LineItem();
        lineItem.setProduct(productService.getProductByName(nameOfProduct));
        lineItem.setBasket(basketService.getBasketByID(id));
        lineItem.setQuantity(1);
        lineItemDAO.saveLineItem(lineItem);

        lineItems.add(lineItem);
        basket.setLineItems(lineItems);
        basketService.editBasket(basket);

        logger.info("basket with added lineitem:" + basket);

        return "redirect:/admin/listBaskets";
    }

    @RequestMapping(path = "/removeItemFromBasket/{id}", method = RequestMethod.GET)
    public String removeItemFromBasket(Model model, @PathVariable("id") Long id){
        logger.info("**********ADMIN CONTROLLER - REMOVE LINE ITEM**********");
        lineItemDAO.removeLineItem(lineItemDAO.getById(id));
        return "redirect:/admin/listBaskets";
    }

    @RequestMapping(path = "/changeQuantity/{basketId}/{productName}", method = RequestMethod.POST)
    public String changeQuantityOfItem(Model model, @PathVariable("basketId") Long id, @PathVariable("productName") String productName,
                                       @RequestParam("quantity") int quantity){
       LineItem lineItem = lineItemDAO.getbyBasketIdAndProductName(id, productName);
       lineItem.setQuantity(quantity);
       lineItemDAO.updateLineItem(lineItem);
       logger.info("adminController: lineItem with id " + id + " is updated:" + lineItem);
       return "redirect:/admin/listBaskets";
    }

    @RequestMapping(path="/uploadFile/{id}", method = RequestMethod.POST)
    public String addPicture(@PathVariable("id") Long id, Model model,  @RequestParam CommonsMultipartFile[] fileUpload){
        pictureService.uploadFile(id, fileUpload);
        return "redirect:/admin/listProducts";
    }

}
