package com.Controllers;

import com.DAO.PictureDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.logging.Logger;

@Controller
@RequestMapping("/")
public class mainController {
    Logger logger = Logger.getLogger("main");

//    EmployeeDAO employeeDAO;
    PictureDAO pictureDAO;

    @Autowired
    public mainController(PictureDAO pictureDAO){
//        this.employeeDAO = employeeDAO;
        this.pictureDAO = pictureDAO;
    }

    @RequestMapping(path = "/home", method = RequestMethod.GET)
    public String getListEmployee(Model model){
//        List<Employee> listEmployee = employeeDAO.list();
//        logger.info("list of employees from DAO:" + listEmployee);
//        model.addAttribute("listEmployee", listEmployee);

        return "userPages/home";
    }
//
//    @RequestMapping(path="/remove/{id}", method = RequestMethod.GET)
//    public String remove(@PathVariable("id") int id, Model model){
//        employeeDAO.remove(id);
//        List<Employee> listEmployee = employeeDAO.list();
//        logger.info("list of employees from DAO:" + listEmployee);
//        model.addAttribute("listEmployee", listEmployee);
//        return "userPages/home";
//    }
//
//    @RequestMapping(path="/edit/{id}", method = RequestMethod.GET)
//    public String edit(@PathVariable("id") int id, Model model){
//        Employee employee = employeeDAO.getById(id);
//
//        logger.info("updating employee:" + employee);
//        model.addAttribute("employee", employee);
//        return "userPages/update";
//    }
//
//    @RequestMapping(path="/edit/{id}", method = RequestMethod.POST)
//    public String editEmployee(@PathVariable("id") int id, Employee employee){
////        Employee employee = employeeDAO.getById(id);
//
//        logger.info("updating employee:" + employee);
//        employeeDAO.update(employee);
//
//        return "userPages/updateSuccess";
//    }
//
//    @RequestMapping(path="/addEmployee", method = RequestMethod.GET)
//    public String add(Model model){
//        Employee employee = new Employee();
//        model.addAttribute("employee", employee);
//        return "userPages/add";
//    }
//
//    @RequestMapping(path="/addEmployee", method = RequestMethod.POST)
//    public String addEmployee(Employee employee){
//        employeeDAO.insert(employee);
//        return "redirect: userPages/home";
//    }
//
//    @RequestMapping(path="/uploadFile/{id}", method = RequestMethod.GET)
//    public String addPict(Model model, @PathVariable("id") int id){
//        Picture picture = new Picture();
//        picture.setEmployeeId(employeeDAO.getById(id));
//        model.addAttribute("picture", picture);
//        model.addAttribute("id", id);
//        return "userPages/upload";
//    }
//
//    @RequestMapping(path="/uploadFile/{id}", method = RequestMethod.POST)
//    public String addPicture(@PathVariable("id") int id, Model model,  @RequestParam CommonsMultipartFile[] fileUpload){
//        //написать как получить bytes from request
//        Picture picture = new Picture();
//        picture.setEmployeeId(employeeDAO.getById(id));
//
//        if (fileUpload != null && fileUpload.length > 0) {
//            for (CommonsMultipartFile aFile : fileUpload){
//
//                System.out.println("Controller - Saving file: " + aFile.getOriginalFilename());
//                picture.setData(aFile.getBytes());
//                pictureDAO.insertPicture(picture);
//            }
//        }
//
//        logger.info("Model in addPicture() controller: " + model.asMap());
//        return "redirect:userPages/home";
//    }

//    @RequestMapping(path="/listPictures/{id}", method = RequestMethod.GET)
//    public String listPict(Model model, @PathVariable("id") int id){
//
//        Employee employee = employeeDAO.getById(id);//.getPicture();
//        Set<Picture> pictures = employee.getPicture();
//        model.addAttribute("pictures", pictures);
//        logger.info("List of pictures for employee with id " + id + ": " + employee.getPicture());
//        return "redirect:/home";
//        return "userPages/pictures";
//    }
}
