package com.Config;

import com.DAO.*;
import com.DAO.DAOImplementation.*;
import com.Model.*;
import com.Service.*;
import com.Service.ServiceImpl.*;
import net.sf.ehcache.CacheManager;
import org.hibernate.SessionFactory;
import org.hibernate.cache.impl.NoCachingRegionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//import org.springframework.cache.CacheManager;



@Configuration
@EnableCaching
@ComponentScan("com")
@PropertySource({"classpath:hibernate.properties", "classpath:database.properties"})
@EnableTransactionManagement
@EnableWebMvc
public class SpringMVCHibernateAnnotationConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private Environment env;

    public SpringMVCHibernateAnnotationConfig(){}

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));//com.mysql.jdbc.Driver
        dataSource.setUrl(env.getProperty("jdbc.url"));//jdbc:mysql://localhost:3306/employeesdb
        dataSource.setUsername(env.getProperty("jdbc.username"));//root
        dataSource.setPassword(env.getProperty("jdbc.password"));//mysql
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource());
        return jdbcTemplate;
    }

    @Bean
    public Properties hibernateProperties()
    {
        Properties properties = new Properties();
        System.out.println("Environment contains username property: " + env.containsProperty("jdbc.username"));
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.cache.region.factory_class", env.getProperty("hibernate.cache.region.factory_class"));
        properties.put("hibernate.generate_statistics", env.getProperty("hibernate.generate_statistics"));
        System.out.println("Properties" + properties);
        return properties;
    }

    @Bean
    public SessionFactory sessionFactory() throws Exception {
        System.out.println("Creating LocalSessionFactoryBean...");

        AnnotationSessionFactoryBean annotationSessionFactoryBean = new AnnotationSessionFactoryBean();
        annotationSessionFactoryBean.setDataSource(dataSource());
        annotationSessionFactoryBean.setPackagesToScan("com.DAO");
        annotationSessionFactoryBean.setAnnotatedClasses(Picture.class, Action.class, Basket.class,
                OrderStatus.class, Product.class, Role.class, User.class, UserStatus.class, LineItem.class, Order.class);
        annotationSessionFactoryBean.setHibernateProperties(hibernateProperties());
        System.out.println("After setting hibernate properties..." + annotationSessionFactoryBean);
        annotationSessionFactoryBean.setCacheRegionFactory(new NoCachingRegionFactory(null));
        annotationSessionFactoryBean.afterPropertiesSet();
//        annotationSessionFactoryBean.setCacheRegionFactory(new NoCachingRegionFactory(null));//new org.hibernate.cache.ehcache.EhCacheRegionFactory());

        System.out.println("AnnotationSessionFactoryBean: " + annotationSessionFactoryBean);
        System.out.println("sessionFactory:" + annotationSessionFactoryBean.getObject());
        return annotationSessionFactoryBean.getObject();

    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) throws Exception {
        System.out.println("Creating transactionManager bean...");
        System.out.println("sessionFactory: " + sessionFactory);
        HibernateTransactionManager transactionManager= new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }

    @Bean
    public ActionDAO actionDAO(){
        ActionDAOImpl actionDAOImpl = new ActionDAOImpl();
        return actionDAOImpl;
    }

    @Bean
    public LineItemDAO lineItemDAO(){
        LineItemDAOImpl lineItemDAO = new LineItemDAOImpl();
        return lineItemDAO;
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(800000);
        return multipartResolver;
    }

    @Bean
    public PictureDAO pictureDAO(){
        PictureDAOImpl pictureDAO = new PictureDAOImpl();
        return pictureDAO;
    }

    @Bean
    public BasketDAO basketDAO(){
        return new BasketDAOImpl();
    }

    @Bean
    public ProductDAO productDAO(){
        return new ProductDAOImpl();
    }

    @Bean
    public UserDAO userDAO(){
        return new UserDAOImpl();
    }

    @Bean
    public OrderDAO orderDAO(){
        return new OrderDAOImpl();
    }

    @Bean
    public ActionService actionService(){
        return new ActionServiceImpl();
    }

    @Bean
    public PictureService pictureService(){
        return new PictureServiceImpl();
    }

    @Bean
    public ProductService productService(){
        return new ProductServiceImpl();
    }

    @Bean
    public UserService userService(){
        return new UserServiceImpl();
    }

    @Bean
    public BasketService basketService(){
        return new BasketServiceImpl();
    }

    @Bean
    public OrderService orderService(){
        return new OrderServiceImpl();
    }

    @Bean
    public LineItemService lineItemService(){
        return new LineItemServiceImpl();
    }

    @Bean
    public EhCacheCacheManager cacheManager(CacheManager cm) {
        return new EhCacheCacheManager(cm);
    }

    @Bean
    public EhCacheManagerFactoryBean ehCacheCacheManager() {
        EhCacheManagerFactoryBean cmfb = new EhCacheManagerFactoryBean();
        cmfb.setConfigLocation(new ClassPathResource("ehcache.xml"));
        cmfb.setCacheManagerName("FirstCacheManager");

        cmfb.setShared(true);
        return cmfb;
    }
//ADDING FUNCTIONALITY WITH PICTURES
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(byteArrayHttpMessageConverter());
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        arrayHttpMessageConverter.setSupportedMediaTypes(getSupportedMediaTypes());
        return arrayHttpMessageConverter;
    }

    private List<MediaType> getSupportedMediaTypes() {
        List<MediaType> list = new ArrayList<MediaType>();
        list.add(MediaType.IMAGE_JPEG);
        list.add(MediaType.IMAGE_PNG);
        list.add(MediaType.APPLICATION_OCTET_STREAM);
        return list;
    }

}
