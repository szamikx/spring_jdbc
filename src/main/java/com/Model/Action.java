package com.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Actions")
public class Action {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "discountPercent")
    private double discountPercent;

    @Column(name = "quantity")
    private int quantity;

    @ManyToMany(fetch = FetchType.EAGER)//, cascade=CascadeType.REMOVE)
    @JoinTable(name = "ProductsActions", joinColumns = {@JoinColumn(name = "idAction", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "idProduct", referencedColumnName = "id")})
    private Set<Product> products = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Action{" +
                "id=" + id +
                "name='" + name + '\'' +
                ", description=" + description +
                ", discountPercent=" + discountPercent +
                ", quantity=" + quantity +
                '}';
    }
}
