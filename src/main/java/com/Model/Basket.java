package com.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "baskets")
public class Basket {

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "basket")
    private List<LineItem> lineItems = new ArrayList<>();

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

//    @Column(name = "orderStatus")
//    @Enumerated(EnumType.STRING)
//    private OrderStatus orderStatus;




    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public OrderStatus getOrderStatus() {
//        return orderStatus;
//    }
//
//    public void setOrderStatus(OrderStatus orderStatus) {
//        this.orderStatus = orderStatus;
//    }

    @Override
    public String toString() {
        List<String> lineItemsList = new ArrayList<>();
        for(LineItem lineItem: lineItems){
            lineItemsList.add(lineItem.getProduct().getNameOfProduct() + lineItem.getQuantity());
        }
        return "Basket{" +
                "user=" + user +
                ", lineItems=" + lineItemsList +
                ", id=" + id +
//                ", orderStatus=" + orderStatus +
                '}';
    }
}
