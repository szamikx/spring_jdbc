package com.Model;


//import org.hibernate.annotations.Entity;

import javax.persistence.*;

@Entity
@Table(name="pictures")
public class Picture {

    @Id
    @Column(name = "pictureId")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long pictureId;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data", nullable=false, columnDefinition = "longblob")
    private byte[] data;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST })//cascade = CascadeType.ALL, targetEntity = Product.class)//, cascade = CascadeType.ALL)
    @JoinColumn(name="productid")
    private Product productId;

    @Column(name = "pictureName")
    private String pictureName;

    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public void setPictureId(Long pictureId) {
        this.pictureId = pictureId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public Long getPictureId() {
        return pictureId;

    }

//    public void setPictureId(Long pictureId) {
//        this.pictureId = pictureId;
//    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

//    public Employee getEmployeeId() {
//        return employeeId;
//    }

//    public void setEmployeeId(Employee employeeId) {
//        this.employeeId = employeeId;
//    }


    @Override
    public String toString() {
        return "Picture{" +
                "pictureId=" + pictureId +
//                ", data=" + Arrays.toString(data) +
                ", productId=" + productId +
                ", pictureName='" + pictureName + '\'' +
                '}';
    }
}
