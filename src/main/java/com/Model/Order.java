package com.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Orders")
public class Order {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST })
    @JoinColumn(name = "userId")
    private User user;

    @Column(name = "orderDate")
    private Date date;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    private List<LineItem> orderItems;

    @Column(name = "orderStatus")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Column(name = "totalAmount")
    private double totalAmount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<LineItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<LineItem> orderItems) {
        this.orderItems = orderItems;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        int size = 0;
        if(this.orderItems != null){
            size = this.orderItems.size();
        }
        return "-----ORDER------{" +
                "id=" + id +
                ", user=" + user.getName() +
                ", date=" + date +
                ", orderStatus=" + orderStatus +
                ", totalAmount=" + totalAmount +
                ", orderItems size=" + size +
                '}';
    }
}
