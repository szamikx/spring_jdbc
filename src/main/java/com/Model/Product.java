package com.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nameOfProduct")
    private String nameOfProduct;

    @Column(name = "descriptionOfProduct")
    private String descriptionOfProduct;

    @OneToMany(cascade = { CascadeType.REMOVE, CascadeType.PERSIST}, fetch = FetchType.EAGER, mappedBy = "productId")
    private Set<Picture> picturesProduct = new HashSet<>();

    @Column(name = "inAction")
    private boolean inAction;

    @ManyToMany(mappedBy = "products", cascade = { CascadeType.REMOVE, CascadeType.PERSIST})
    private Set<Action> actions = new HashSet<>();

    @Column(name = "price")
    private double price;



    public Long getId() {
        return id;
    }

    public void addAction(Action action){
        actions.add(action);
    }

    public void removeAction(Action action){
        actions.remove(action);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Action> getActions() {
        return actions;
    }

    public void setActions(Set<Action> actions) {
        this.actions = actions;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public String getDescriptionOfProduct() {
        return descriptionOfProduct;
    }

    public void setDescriptionOfProduct(String descriptionOfProduct) {
        this.descriptionOfProduct = descriptionOfProduct;
    }

    public Set<Picture> getPicturesProduct() {
        return picturesProduct;
    }

    public void setPicturesProduct(Set<Picture> picturesProduct) {
        this.picturesProduct = picturesProduct;
    }

    public boolean isInAction() {
        return inAction;
    }

    public void setInAction(boolean inAction) {
        this.inAction = inAction;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "nameOfProduct='" + nameOfProduct +
                ", descriptionOfProduct=" + descriptionOfProduct +
//                ", picturesProduct=" + picturesProduct +
//                ", inAction=" + inAction +
//                ", actionList=" + actions +
                ", price=" + price +
                '}';
    }
}
