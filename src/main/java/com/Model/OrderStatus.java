package com.Model;

public enum OrderStatus {
    RESERVED, PAID, SHIPPED, RECEIVED, CLOSED;
}
