package com.Service;

import com.Model.Basket;
import com.Model.Order;

import java.util.List;

public interface OrderService {

    List<Order> getOrderList();
    Order getOrderById(Long orderId);
    void saveOrder(Order order);
    void updateOrder(Order order);
    void deleteOrder(Long orderId);
    double calculateOrder(Basket basket);
    Order createOrderFromBasket(Basket basket);
}
