package com.Service.ServiceImpl;

import com.DAO.BasketDAO;
import com.DAO.LineItemDAO;
import com.Model.Basket;
import com.Model.LineItem;
import com.Service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class BasketServiceImpl implements BasketService {

    Logger logger = Logger.getLogger("main");

    @Autowired
    BasketDAO basketDAO;

    @Autowired
    LineItemDAO lineItemDAO;

    @Override
    public void editBasket(Basket basket) {
        basketDAO.updateBasket(basket);
        logger.info("BasketService - editBasket - updated.." + basket);
    }

    @Override
    public void cleanBasket(Long basketId) {
        Basket basket = basketDAO.getBasketByID(basketId);
        basket.setLineItems(new ArrayList<LineItem>());
        basketDAO.updateBasket(basket);
        logger.info("BasketService - cleanBasket - cleaned.." + basket);
    }

    @Override
    public List<Basket> listBaskets() {
        List<Basket> basketList = basketDAO.getBasketList();
        if(basketList != null){
            logger.info("Basket service - get List - basketList is found:" + basketList);
        } else {
            logger.info("Basket service - get List - basketList is not found");
        }
        return basketList;
    }

    @Override
    public void removeBasket(Long basketId) {
        basketDAO.removeBasket(basketDAO.getBasketByID(basketId));
        if(basketDAO.getBasketByID(basketId) == null){
            logger.info("BasketService - removeBasket - basket is removed..");
        } else {
            logger.info("BasketService - removeBasket - basket is not removed..");
        }
    }

    @Override
    public void addItemToBasket(Long basketId, LineItem item) {
        Basket basket = basketDAO.getBasketByID(basketId);
        List<LineItem> lineItems = basket.getLineItems();
        int size = lineItems.size();
        lineItems.add(item);
        basket.setLineItems(lineItems);
        basketDAO.updateBasket(basket);
        if(size < basket.getLineItems().size()){
            logger.info("BasketService - addItemToBasket - added, lineItems:" + basket.getLineItems());
        } else {
            logger.info("BasketService - addItemToBasket - NOT added, lineItems:" + basket.getLineItems());
        }

    }

    @Override
    public void removeItemFromBasket(Long basketId, LineItem item) {
        Basket basket = basketDAO.getBasketByID(basketId);
        List<LineItem> lineItems = basket.getLineItems();
        boolean isRemoved = lineItems.remove(item);
        if(isRemoved) {
            basket.setLineItems(lineItems);
            logger.info("BasketService - removeItemFromBasket - is removed, lineItems:" + basket.getLineItems());
        } else {
            logger.info("BasketService - removeItemFromBasket - is NOT removed, lineItems:"  + basket.getLineItems());
        }
    }

    @Override
    public void changeQuantity(Long basketId, int quantity, LineItem item) {
    }

    @Override
    public Basket getBasketByID(Long basketId) {
        Basket basket = basketDAO.getBasketByID(basketId);
        if(basket != null) {
            logger.info("BasketService - getBasketById - basket is found: " + basket);
            return basket;
        } else {
            logger.info("BasketService - getBasketById - basket is NOT found: " + basket);
            return null;
        }
    }
}
