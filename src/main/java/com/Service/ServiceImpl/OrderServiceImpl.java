package com.Service.ServiceImpl;

import com.DAO.LineItemDAO;
import com.DAO.OrderDAO;
import com.Model.Basket;
import com.Model.LineItem;
import com.Model.Order;
import com.Model.OrderStatus;
import com.Service.OrderService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class OrderServiceImpl implements OrderService {
    Logger logger = Logger.getLogger("main");

    @Autowired
    OrderDAO orderDAO;

    @Autowired
    LineItemDAO lineItemDAO;

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<Order> getOrderList() {
//        Session session = sessionFactory.getCurrentSession();
        List<Order> orderList = orderDAO.listOrders();
        if(orderList.size() > 0){
            return orderList;
        } else {
            logger.info("OrderService - getOrderList - not found..");
            return null;
        }
    }

    @Override
    public Order getOrderById(Long orderId) {
        Order order = orderDAO.getOrderById(orderId);
        if(order != null){
            return order;
        } else {
            logger.info("OrderService - getOrderById - not found..");
            return null;
        }
    }

    @Override
    public void saveOrder(Order order) {
        orderDAO.saveOrder(order);
        if(order.getId() > 0){
            logger.info("OrderService - saveOrder - saved.." + order);
        } else {
            logger.info("OrderService - saveOrder - not saved..");
        }

    }

    @Override
    public void updateOrder(Order order) {
        orderDAO.updateOrder(order);
        logger.info("OrderService - updateOrder - updated..");
    }

    @Override
    public void deleteOrder(Long orderId) {
        orderDAO.deleteOrder(orderId);
        if(orderDAO.getOrderById(orderId) != null){
            logger.info("OrderService - deleteOrder - not deleted..");
        } else {
            logger.info("OrderService - deleteOrder - deleted..");
        }
    }

    @Override
    public double calculateOrder(Basket basket) {
        double totalAmount = 0;
        List<LineItem> lineItemList = basket.getLineItems();
        for(LineItem li: lineItemList){
            if(li.getProduct().isInAction()){
                //calculate quantity;
                //define actions for this product;
                //if quantity is ok for some action/actions, if yes - take the discount for first action
                //calculate of price of product with this discount
//                totalAmount +=
            }
            totalAmount += li.getQuantity() * li.getProduct().getPrice();
        }
        return totalAmount;
    }

    @Override
    public Order createOrderFromBasket(Basket basket) {
        //making new Order from basket
        Order order = new Order();
        order.setUser(basket.getUser());
        order.setDate(new Date());
        order.setOrderStatus(OrderStatus.RESERVED);
        double amountOfOrder = this.calculateOrder(basket);
        order.setTotalAmount(amountOfOrder);
        order.setOrderItems(basket.getLineItems());
        orderDAO.saveOrder(order);
        logger.info("order is saved: " + order);

        //saving order in LineItems
        List<LineItem> lineItemList = basket.getLineItems();
        for(LineItem li: lineItemList){
            logger.info("*****inside ORDERSERVICEIMPL - CREATEORDERFROMBASKET - lineItem.id: " + li.getId());
            li.setOrder(order);
            lineItemDAO.updateLineItem(li);
            logger.info("*****inside ORDERSERVICEIMPL - CREATEORDERFROMBASKET - lineItem updated, lineItem.order:" + li.getOrder().getId());
        }

        //setting lineItems in Order
//        order.setOrderItems(lineItemList);

//        this.updateOrder(order);
//        logger.info("ORDER SERVICE - CREATE ORDER FROM BASKET - Amount of lineirems in order is :");
        logger.info("*****inside ORDERSERVICEIMPL - CREATEORDERFROMBASKET - line items in order:" + order.getOrderItems());
        return order;
    }
}
