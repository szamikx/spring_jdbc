package com.Service.ServiceImpl;

import com.DAO.LineItemDAO;
import com.Model.LineItem;
import com.Service.LineItemService;
import com.Service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.logging.Logger;

public class LineItemServiceImpl implements LineItemService {


    Logger logger = Logger.getLogger("main");

    @Autowired
    LineItemDAO lineItemDAO;

    @Autowired
    OrderService orderService;

    @Override
    public void setOrderId(Long orderId, Long lineItemId) {
        LineItem lineItem = lineItemDAO.getById(lineItemId);
        lineItem.setOrder(orderService.getOrderById(orderId));
//        lineItem.setBasket(null);
        lineItemDAO.updateLineItem(lineItem);
    }

    @Override
    public List<LineItem> getLineItemsByBasketId(Long basketId) {
        return lineItemDAO.getbyBasketId(basketId);

    }
}
