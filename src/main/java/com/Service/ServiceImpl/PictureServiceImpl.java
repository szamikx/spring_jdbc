package com.Service.ServiceImpl;

import com.DAO.PictureDAO;
import com.DAO.ProductDAO;
import com.Model.Picture;
import com.Service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;
import java.util.logging.Logger;

public class PictureServiceImpl implements PictureService {

    @Autowired
    PictureDAO pictureDAO;

    @Autowired
    ProductDAO productDAO;

    Logger logger = Logger.getLogger("main");

    @Override
    public List<Picture> listPictures() {
        List<Picture> listPictures = pictureDAO.listPicture();
        return listPictures;
    }

    @Override
    public byte[] getImagesForProduct(Long pictureId) {
        byte[] pictureData = pictureDAO.getPictureById(pictureId).getData();
        logger.info("PictureService - pictureData: " + pictureData);
        return pictureData;
    }

    @Override
    public void removePicture(Long pictureId) {
        pictureDAO.removePicture(pictureDAO.getPictureById(pictureId));
        logger.info("PictureService: picture is removed, id:" + pictureId);
    }

    @Override
    public Picture getPictureById(Long pictureId) {
        Picture picture = pictureDAO.getPictureById(pictureId);
        return picture;
    }

    @Override
    public void updatePicture(Picture picture) {
        if(picture != null) {
            pictureDAO.updatePicture(picture);
        } else {
            logger.info("PictureService - updatePicture - argument is null");
        }
    }

    @Override
    public void uploadFile(Long productId, CommonsMultipartFile[] fileUpload) {
        Picture picture = new Picture();
        picture.setProductId(productDAO.getProductById(productId));

        if (fileUpload != null && fileUpload.length > 0) {
            for (CommonsMultipartFile aFile : fileUpload){

                logger.info("PictureService - Saving file: " + aFile.getOriginalFilename());
                picture.setData(aFile.getBytes());
                pictureDAO.insertPicture(picture);
            }
    }



    }
}

//    @RequestMapping(path="/uploadFile/{id}", method = RequestMethod.POST)
//    public String addPicture(@PathVariable("id") int id, Model model, @RequestParam CommonsMultipartFile[] fileUpload){
//        //написать как получить bytes from request
//        Picture picture = new Picture();
//        picture.setProductId(productDAO.getProductById(id));
//
//        if (fileUpload != null && fileUpload.length > 0) {
//            for (CommonsMultipartFile aFile : fileUpload){
//
//                logger.info("admin controller - Saving file: " + aFile.getOriginalFilename());
//                picture.setData(aFile.getBytes());
//                pictureDAO.insertPicture(picture);
//            }
//        }