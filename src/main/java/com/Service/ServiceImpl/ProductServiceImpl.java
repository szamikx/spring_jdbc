package com.Service.ServiceImpl;

import com.DAO.ProductDAO;
import com.Model.Product;
import com.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.logging.Logger;

public class ProductServiceImpl implements ProductService{

    Logger logger = Logger.getLogger("main");

    @Autowired
    ProductDAO productDAO;

    @Override
    public List<Product> listProducts() {
        List<Product> productList = productDAO.getListOfProducts();
        if(productList != null){
            return productList;
        }
        return null;
    }

    @Override
    public void addProduct(Product product) {
        productDAO.saveProduct(product);
        if(productDAO.getProductByName(product.getNameOfProduct()) != null) {
            logger.info("ProductService - product is added: " + productDAO.getProductByName(product.getNameOfProduct()));
        }
    }

    @Override
    public void removeProduct(Long productId) {
        productDAO.removeProduct(productDAO.getProductById(productId));
        if (productDAO.getProductById(productId) == null){
            logger.info("ProductService - remove product - product with id " + productId + " is removed.");
            return;
        }
        logger.info("product is not removed..");
    }

    @Override
    public void updateProduct(Product product) {
        productDAO.updateProduct(product);
    }

    @Override
    public Product getProductById(Long id) {
        Product product = productDAO.getProductById(id);
        if(product != null){
            logger.info("ProductService - getProductById - product is found: " + product);
            return product;
        }
        return null;
    }

    @Override
    public Product getProductByName(String nameOfProduct) {
        Product product = productDAO.getProductByName(nameOfProduct);
        if(product != null){
            logger.info("ProductService- getProductByName - product is found:" + product);
            return product;
        }
        return null;
    }
}
