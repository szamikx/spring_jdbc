package com.Service.ServiceImpl;

import com.DAO.ActionDAO;
import com.DAO.ProductDAO;
import com.Model.Action;
import com.Service.ActionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.logging.Logger;

public class ActionServiceImpl implements ActionService{
    Logger logger = Logger.getLogger("main");

    @Autowired
    ActionDAO actionDAO;

    @Autowired
    ProductDAO productDAO;

    @Override
    public List<Action> listActions() {
        List<Action> listActions = actionDAO.listActions();
        logger.info("list of actions from DAO:" + listActions);
        return listActions;
    }

    @Override
    public void addProductToAction(Long actionId, String productName) {
        Action action = actionDAO.getActionById(actionId);
        if(action != null) {
            actionDAO.addProductToAction(action, productDAO.getProductByName(productName));
        } else {
            logger.info("ActionService: there is no such action...");
        }
    }

    @Override
    public void removeProductFromAction(Long actionId, Long productId) {
        Action action = actionDAO.getActionById(actionId);
        if(action != null) {
            actionDAO.removeProductFromAction(action, productDAO.getProductById(productId));
        } else {
            logger.info("ActionService: there is no such action...");
        }
    }

    @Override
    public void removeAction(Long actionId) {
        actionDAO.deleteAction(actionDAO.getActionById(actionId));
        logger.info("Action is removed:" + actionDAO.getActionById(actionId));
    }

    @Override
    public Action getActionById(Long id) {
        Action action = actionDAO.getActionById(id);
        if(action != null){
            return action;
        }
        return null;
    }

    @Override
    public Action getActionByName(String actionName) {
        Action action;
        if(actionName!=null){
            action = actionDAO.getActionByName(actionName);
        } else {
            logger.info("there is no name of Action as parameter..");
            return null;
        }
        return action;
    }

    @Override
    public void editAction(Action action) {
        Action action1 = this.getActionByName(action.getName());
        if(action1 != null){
            action1.setDescription(action.getDescription());//
            action1.setDiscountPercent(action.getDiscountPercent());
            action1.setName(action.getName());
            action1.setProducts(action.getProducts());
            action1.setQuantity(action.getQuantity());
            actionDAO.editAction(action1);
            logger.info("action is updated: " + action1);
        } else {
            logger.info("action is not found by name.." + action.getName());
        }
    }

    @Override
    public void addAction(Action action) {
        actionDAO.saveAction(action);
        logger.info("Action is saved:" + actionDAO.getActionByName(action.getName()));
    }
}
