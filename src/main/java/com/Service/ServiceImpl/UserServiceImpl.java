package com.Service.ServiceImpl;

import com.DAO.UserDAO;
import com.Model.Role;
import com.Model.User;
import com.Model.UserStatus;
import com.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.logging.Logger;

public class UserServiceImpl implements UserService {
    Logger logger = Logger.getLogger("main");

    @Autowired
    UserDAO userDAO;

    @Override
    public List<User> listUsers() {
        List<User> userList = userDAO.getListOfUsers();
        if(userList != null){
            logger.info("UserService -listUsers - list users:" + userList);
            return userList;
        } else return null;
    }

    @Override
    public void removeUser(Long id) {
        userDAO.removeUser(userDAO.getUserById(id));
        if(userDAO.getUserById(id) == null)
            logger.info("UserService - removeUser - user with id " + id + " is removed");
        else logger.info("UserService - removeUser - user is not removed..");
    }

    @Override
    public void deactivateUser(Long id) {
        User user = userDAO.getUserById(id);
        if(user != null){
            user.setStatus(UserStatus.NonActive);
            userDAO.updateUser(user);
            logger.info("UserService - deactivateUser - user is deactivated..");
        } else {
            logger.info("UserService - deactivateUser - user is not deactivated..");
        }
    }

    @Override
    public void activateUser(Long id) {
        User user = userDAO.getUserById(id);
        if(user != null){
            user.setStatus(UserStatus.Active);
            userDAO.updateUser(user);
            logger.info("UserService - deactivateUser - user is activated..");
        } else {
            logger.info("UserService - deactivateUser - user is not activated..");
        }
    }

    @Override
    public void changeRole(Long userId, Role role) {
        User user = userDAO.getUserById(userId);
        if(user != null){
            user.setRole(role);
            userDAO.updateUser(user);
            logger.info("UserService - changeRole - userRole is changed..");
        } else {
            logger.info("UserService - changeRole - userRole is not changed..");
        }
    }

    @Override
    public void addUser(User user) {
        userDAO.saveUser(user);
        if(user.getId() != 0){
            logger.info("UserService - addUser - user is saved:" + user);
        } else {
            logger.info("UserService - addUser - user is not saved");
        }
    }

    @Override
    public void updateUser(User user) {
        userDAO.updateUser(user);
        logger.info("UserService - updateUser - user is updated");
    }

    @Override
    public User getUserById(Long id) {
        User user = userDAO.getUserById(id);
        if(user != null){
            logger.info("UserService - getUserById - user is found: " + user);
            return user;
        } else {
            logger.info("UserService - getUserById - user is not found");
            return null;
        }
    }
}
