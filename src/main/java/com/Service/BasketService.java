package com.Service;

import com.Model.Basket;
import com.Model.LineItem;

import java.util.List;

public interface BasketService {

    public List<Basket> listBaskets();
    public void editBasket(Basket basket);
    void removeBasket(Long basketId);
    void addItemToBasket(Long basketId, LineItem item);
    void removeItemFromBasket(Long basketId, LineItem item);
    void changeQuantity(Long basketId, int quantity, LineItem item);
    Basket getBasketByID(Long basketId);
    void cleanBasket(Long basketId);

}
