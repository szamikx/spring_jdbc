package com.Service;

import com.Model.Action;

import java.util.List;

public interface ActionService {
    List<Action>  listActions();
    void addProductToAction(Long actionId, String productName);
    void  removeProductFromAction(Long actionId, Long productId);///{id}/{productId}"
    void removeAction(Long actionId);///{id}
    void editAction(Action action);///{id}
    void addAction(Action action);//
    Action getActionById(Long id);
    Action getActionByName(String actionName);
}
