package com.Service;

import com.Model.Product;

import java.util.List;

public interface ProductService {
    List<Product> listProducts();
    void addProduct(Product product);
    void removeProduct(Long productId);
    void updateProduct(Product product);
    Product getProductById(Long id);
    Product getProductByName(String nameOfProduct);

}
