package com.Service;

import com.Model.Role;
import com.Model.User;

import java.util.List;

public interface UserService {
    List<User> listUsers();
    void removeUser(Long id);
    void deactivateUser(Long id);
    void activateUser(Long id);
    void changeRole(Long userId, Role role);
    void addUser(User user);
    void updateUser(User user);
    User getUserById(Long id);
}
