package com.Service;

import com.Model.LineItem;

import java.util.List;

public interface LineItemService {
    void setOrderId(Long orderId, Long lineItem);
    List<LineItem> getLineItemsByBasketId(Long basketId);
}
