package com.Service;

import com.Model.Picture;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.util.List;

public interface PictureService {
    List<Picture> listPictures();
    byte[] getImagesForProduct(Long pictureId);
    void removePicture(Long pictureId);
    void uploadFile(Long productId, CommonsMultipartFile[] fileUpload);
    Picture getPictureById(Long pictureId);
    void updatePicture(Picture picture);
}
